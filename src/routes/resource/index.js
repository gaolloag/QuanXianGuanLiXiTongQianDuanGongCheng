import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'dva'
import { routerRedux } from 'dva/router'
import { Page } from 'components'
import List from './List'
import Filter from './Filter'
import Modal from './Modal'

const Resource = ({ location, dispatch, resource, loading  }) =>{
  const {list, pagination, currentItem, modalVisible, modalType, isMotion, selectedRowKeys} = resource
  const {pageSize} = pagination

  /* 列表props */
  const listProps = {
    dataSource: list,
    loading: loading.effects['resource/query'],
    pagination,
    location,
    isMotion,
    onChange(page) {
      const {query, pathname} = location
      dispatch(routerRedux.push({
        pathname,
        query: {
          ...query,
          page: page.current,
          pageSize: page.pageSize,
        },
      }))
    },
    onDeleteItem(id) {
      dispatch({
        type: 'resource/delete',
        payload: id,
      })
    },
    onEditItem(item) {
      dispatch({
        type: 'resource/showModal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      })
    },
    rowSelection: {
      selectedRowKeys,
      onChange: (keys) => {
        dispatch({
          type: 'resource/updateState',
          payload: {
            selectedRowKeys: keys,
          },
        })
      },
    },
  }

  /* 查询 props */
  const filterProps = {
    isMotion,
    filter: {
      ...location.query,
    },
    onFilterChange (value) {
      dispatch(routerRedux.push({
        pathname: location.pathname,
        query: {
          ...value,
          page: 1,
          pageSize,
        },
      }))
    },
    onSearch (fieldsValue) {
      fieldsValue.keyword.length ? dispatch(routerRedux.push({
        pathname: '/resource',
        query: {
          field: fieldsValue.field,
          keyword: fieldsValue.keyword,
        },
      })) : dispatch(routerRedux.push({
        pathname: '/resource',
      }))
    },
    onAdd () {
      dispatch({
        type: 'resource/showModal',
        payload: {
          modalType: 'create',
        },
      })
    },
    switchIsMotion () {
      dispatch({ type: 'resource/switchIsMotion' })
    },
  }

  /* 创建 props  */
  const modalProps = {
    visible: modalVisible,
    maskClosable: false,
    confirmLoading: loading.effects['user/update'],
    title: '新建资源',
    wrapClassName: 'vertical-center-modal',
    onOk (data) {
      dispatch({
        type: `resource/${modalType}`,
        payload: data,
      })
    },
    onCancel () {
      dispatch({
        type: 'resource/hideModal',
      })
    },
  }

  return (
    <Page inner>

      <Filter {...filterProps} />
      {
        selectedRowKeys.length > 0 &&
        <Row style={{ marginBottom: 24, textAlign: 'right', fontSize: 13 }}>
          <Col>
            {`Selected ${selectedRowKeys.length} items `}
            <Popconfirm title={'Are you sure delete these items?'} placement="left" onConfirm={handleDeleteItems}>
              <Button type="primary" size="large" style={{ marginLeft: 8 }}>Remove</Button>
            </Popconfirm>
          </Col>
        </Row>
      }

      <List {...listProps} />

      {modalVisible && <Modal {...modalProps} />}
    </Page>
  )

}

Resource.prototype = {
  resource: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
}

export default connect(({ resource, loading }) => ({ resource, loading }))(Resource)
